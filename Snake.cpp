#include "Snake.h"
#include <random>
#include <termios.h>


int RandomNum(int from, int to) {
    std::random_device generator;
    std::uniform_int_distribution<int> distribution(from, to - 1);
    return distribution(generator);
}

//===============================================================================

Ways::Ways() {
}

Ways::Ways(const directions& d) {
    way = d;
}

int Ways::getValue() {
    return way;
}

int Ways::operator+(const int & a) {

    int sum = this->way + a;
    if (sum >= 4)
        sum %= 2;
    else
    if (sum < 0)
        std::cout << "ERROR bad way";

    return sum;

}

//===============================================================================

Area::Area(int horizontally, int vertical) {
    x = horizontally;
    y = vertical;
}

//===============================================================================

Snake::Snake(int x, int y, int snakeSize) {
    direction = ' ';
    way =  way.UP;
    for (int i = 0; i < snakeSize; i++)
        snakeBody.push_back({y, x + i});
}

bool Snake::isCrash() {

    for (auto iter = snakeBody.begin() + 1; iter < snakeBody.end(); iter++)
        if (snakeBody.front().y == iter->y && snakeBody.front().x == iter->x)
            return true;
    return false;
}

bool Snake::isWall(Area a) {
    return getSnakeBody().front().x == a.x ||
        getSnakeBody().front().x < 0 ||
        getSnakeBody().front().y == a.y ||
        getSnakeBody().front().y < 0;
}

bool Snake::isReturn(Ways way) {
    if (way + 2 == this->way.getValue())
        return true;
    this->way = way;
    return false;

}

void Snake::grow() {
    snakeBody.push_back(snakeBody.back());
}

void Snake::move() {
    snakeBody.insert(++snakeBody.begin(), snakeBody.back());
    snakeBody[1] = head;
    snakeBody.erase(--snakeBody.end());
}

void Snake::lastWayMove() {
     void (Snake::*moveMethods[4])(void) {
         &Snake::ahead, &Snake::right, &Snake::back, &Snake::left
     };
    (this->*moveMethods[way.getValue()])();
}

std::istream& operator>>(std::istream& is, Snake & ch) {
    termios term;
    tcgetattr(0, &term);
    term.c_lflag &= ~ICANON;         // wyłącza buffer
    term.c_lflag &= ~ECHO;           // ukrywa znak
    tcsetattr(0, TCSANOW, &term);
    is >> ch.direction;
    return is;
}

//===============================================================================

SnakeControl::SnakeControl(int x, int y, int snakeSize) : Snake(x, y, snakeSize) {
}

SnakeControl::SnakeControl(Snake &s) : Snake(s) {
}

void SnakeControl::ahead() {
    if (isReturn(way.UP))
        return;
    head = snakeBody.front();
    snakeBody.front().y--;
    move();
}

void SnakeControl::back() {
    if (isReturn(way.DOWN))
        return;
    head = snakeBody.front();
    snakeBody.front().y++;
    move();
}

void SnakeControl::left() {
    if (isReturn(way.LEFT))
        return;
    head = snakeBody.front();
    snakeBody.front().x--;
    move();
}

void SnakeControl::right() {
    if (isReturn(way.RIGHT))
        return;
    head = snakeBody.front();
    snakeBody.front().x++;
    move();
}

//===============================================================================

foods::foods(Area &a) {
    food.x = RandomNum(0, a.x);
    food.y = RandomNum(0, a.y);
}

int& foods::getFood(int i) {
    int *p[] {&food.x, &food.y};
    return *p[i];
}

bool foods::isEaten(Snake &s) {
    return s.getSnakeBody().front().x == food.x && s.getSnakeBody().front().y == food.y;
}

//===============================================================================

draw::draw(const Area &a) {
    drawArea.y = a.y;
    drawArea.x = a.x;
    chars = new char*[drawArea.y];
    for(int i = 0; i < drawArea.y; ++i)
        chars[i] = new char[drawArea.x];
}

draw::~draw() {
    for (int i = 0; i < drawArea.y; ++i)
        delete [] chars[i];
    delete [] chars;
}

void draw::show() const {
    int i, j;
    for (i = 0; i < drawArea.y; i++) {
        for (j = 0; j < drawArea.x; j++)
            std::cout << chars[i][j];
        std::cout << std::endl;
    }
}

void draw::setSnake(Snake &s) {
    char head[] = {'^', '>', 'v', '<'};
    int i, j;
    for (i = 0; i < drawArea.y; i++)
        for (j = 0; j < drawArea.x; j++)
            for (auto S = s.getSnakeBody().begin(); S < s.getSnakeBody().end(); S++)
                if (i == s.getSnakeBody().front().y && j == s.getSnakeBody().front().x)
                        chars[i][j] = head[s.way.getValue()];
                else
                    if (i == S->y && j == S->x)
                        chars[i][j] = 'o';
}

void draw::setArea() {
    int i, j;
    for (i = 0; i < drawArea.y; i++)
        for (j = 0; j < drawArea.x; j++)
            chars[i][j] = ' ';
}

void draw::setFood(foods &f) {
    int i, j;
    for (i = 0; i < drawArea.y; i++)
        for (j = 0; j < drawArea.x; j++)
            if (i == f.getFood(y) && j == f.getFood(x))
                chars[i][j] = '*';
}
