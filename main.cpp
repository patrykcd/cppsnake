#include <cctype>
#include "Snake.h"

#include <chrono>
#include <thread>

int main() {
    SnakeControl s(4, 3);
    Area a;
    draw d(a);
    foods *f = new foods(a);
    while (s.getDirection() != 27) {
        d.setArea();
        d.setSnake(s);
        if (f->isEaten(s)) {
            delete f;
            f = new foods(a);
            s.grow();
        }
        d.setFood(*f);
        d.show();
        if (s.isCrash() || s.isWall(a))
            break;
        std::cin >> s;
        switch (toupper(s.getDirection())) {
            case 'W':
                s.ahead();
                break;
            case 'S':
                s.back();
                break;
            case 'A':
                s.left();
                break;
            case 'D':
                s.right();
                break;
            default:
                s.lastWayMove();
        };
        std::cout << "\033[0;0H";
    }
    delete f;
    return 0;
}
