#ifndef SNAKE_H_INCLUDED
#define SNAKE_H_INCLUDED
#include <iostream>
#include <vector>

class Ways {
    public:
        enum directions : int {UP, RIGHT, DOWN, LEFT};

    private:
        directions way;

    public:
        Ways();
        Ways(const directions& d);
        int getValue();
        int operator+(const int& a);
};

class Area {
    public:
        int x, y;
        Area(int horizontally = 80, int vertical = 23);
};

class Snake {
    private:
        char direction;

    public:
        Ways way;

    protected:
        std::vector <Area>snakeBody;
        Area head;

    public:
        Snake(int x, int y, int snakeSize = 3);
        bool isCrash();
        bool isWall(Area a);
        bool isReturn(Ways way);
        void grow();
        void move();
        void lastWayMove();
        virtual void ahead() = 0;
        virtual void back() = 0;
        virtual void left() = 0;
        virtual void right() = 0;
        std::vector <Area>& getSnakeBody() {return snakeBody;};
        const char& getDirection() const {return direction;};
        friend std::istream& operator>>(std::istream& is,  Snake & ch);
};

class SnakeControl : public Snake {
    public:
        SnakeControl(int x, int y, int snakeSize = 3);
        SnakeControl(Snake &s);
        virtual void ahead() override;
        virtual void back() override;
        virtual void left() override;
        virtual void right() override;
};

class foods {
    private:
        Area food;

    public:
        foods(Area &a);
        int& getFood(int i);
        bool isEaten(Snake &s);
};

class draw {
    private:
        enum {x, y};
        char **chars;
        Area drawArea;

    public:
        draw(const Area &a);
        ~draw();
        void show() const;
        void setSnake(Snake &s);
        void setArea();
        void setFood(foods &f);
};

#endif // SNAKE_H_INCLUDED
